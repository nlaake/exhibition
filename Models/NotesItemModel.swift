//
//  NotesItemModel.swift
//  list
//
//  Created by Nolan on 5/8/24.
//

import Foundation

struct NotesItemModel: Identifiable, Codable {
    let id: String
    let title: String
    let isCompleted: Bool
    
    init(id: String = UUID().uuidString, title: String, isCompleted: Bool) {
        self.id = id
        self.title = title
        self.isCompleted = isCompleted
    }
    
    func updateCompletion() -> NotesItemModel {
        return NotesItemModel(id: id, title: title, isCompleted: !isCompleted)
    }
    
}
